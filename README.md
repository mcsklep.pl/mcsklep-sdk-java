# McSklep SDK Java
Implementation of public API of mcsklep in java

## Installation

### Requirements
- Java 1.8+

### Maven users
Add this dependency and repository to your project

```xml
<dependencies>
    <dependency>
        <groupId>pl.mcsklep</groupId>
        <artifactId>mcsklep-sdk-java</artifactId>
        <version>0.0.7</version>
    </dependency>
</dependencies>


<repositories>
    <repository>
        <id>goxy-maven</id>
        <url>https://repo.goxy.pl/releases</url>
    </repository>
</repositories>
```

### Gradle users
Add this dependency and repository to your project

```groovy
repositories {
    maven {
        url "https://repo.goxy.pl/releases"
    }
}

dependencies {
    compile "pl.mcsklep:mcsklep-sdk-java:0.0.7"
}
```

## Usage
```java
import pl.mcsklep.sdk.exception.ShopErrorException;
import pl.mcsklep.sdk.model.response.action.ActionResponse;
import pl.mcsklep.sdk.model.response.action.ActionStatus;
import pl.mcsklep.sdk.model.response.order.OrderResponse;
import pl.mcsklep.sdk.model.response.order.OrderStatus;

import java.io.IOException;
import java.util.List;

public class ClientExample
{
    public static void main(String[] args)
    {
        ShopClient client = new ShopClient("api_key");

        try
        {
            List<OrderResponse> orders = client.getOrdersByStatus(OrderStatus.PAYED);
            for (OrderResponse order : orders)
            {
                for (ActionResponse action : order.getActions())
                {
                    if (action.getStatus() == ActionStatus.PENDING)
                    {
                        client.completeAction(action.getId());
                    }
                }
            }
        }
        catch (IOException | ShopErrorException e)
        {
            e.printStackTrace();
        }
    }
}
```