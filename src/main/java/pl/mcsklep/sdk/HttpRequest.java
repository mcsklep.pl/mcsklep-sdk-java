package pl.mcsklep.sdk;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import pl.mcsklep.sdk.exception.ShopErrorException;
import pl.mcsklep.sdk.model.response.ErrorResponse;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Map;

public class HttpRequest
{
    private static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    private final ObjectMapper objectMapper;
    private final OkHttpClient client;

    private final String baseUrl;
    private final String token;

    public HttpRequest(String baseUrl, String token)
    {
        this.objectMapper = new ObjectMapper();
        this.client = new OkHttpClient();

        this.baseUrl = baseUrl;
        this.token = token;
    }

    public <T> T get(String path, JavaType returnClass) throws IOException, ShopErrorException
    {
        Request request = this.buildRequest().url(this.baseUrl + path).build();
        return this.call(request, returnClass);
    }

    private Request.Builder buildRequest()
    {
        return new Request.Builder().addHeader("Authorization", String.format("Bearer %s", this.token))
                .addHeader("User-Agent", "mcsklep-sdk-java/${version}");
    }

    private <T> T call(Request request, Type type) throws IOException, ShopErrorException
    {
        try (Response response = this.client.newCall(request).execute())
        {
            ResponseBody body = response.body();
            if (body == null)
            {
                throw new ShopErrorException(-1, "Unexpected error");
            }
            if (!response.isSuccessful())
            {
                try (Reader reader = body.charStream())
                {
                    ErrorResponse error = this.objectMapper.readValue(reader, ErrorResponse.class);
                    if (error != null)
                    {
                        throw new ShopErrorException(error.getCode(), error.getMessage());
                    }
                    else
                    {
                        throw new ShopErrorException(-2, "Undefined error with status code " + response.code());
                    }
                }
            }
            if (type == null)
            {
                return null;
            }
            try (Reader reader = body.charStream())
            {
                return this.objectMapper.readValue(reader, this.objectMapper.constructType(type));
            }
        }
    }

    public <T> T get(String path, Map<String, String> queryParams, Type type) throws IOException, ShopErrorException
    {
        HttpUrl.Builder urlBuilder = HttpUrl.get(this.baseUrl + path).newBuilder();
        queryParams.forEach(urlBuilder::addQueryParameter);

        Request request = this.buildRequest().url(urlBuilder.build()).build();
        return this.call(request, type);
    }

    public <T> T post(String path, Object object, Type type) throws IOException, ShopErrorException
    {
        String json = object != null ? this.objectMapper.writeValueAsString(object) : "";
        RequestBody body = RequestBody.create(json, JSON);

        Request request = this.buildRequest().url(this.baseUrl + path).post(body).build();
        return this.call(request, type);
    }
}
