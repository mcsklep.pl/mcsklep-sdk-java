package pl.mcsklep.sdk;

import com.fasterxml.jackson.core.type.TypeReference;
import pl.mcsklep.sdk.exception.ShopErrorException;
import pl.mcsklep.sdk.model.response.order.OrderResponse;
import pl.mcsklep.sdk.model.response.order.OrderStatus;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ShopClient
        implements ShopSdk
{
    private final HttpRequest request;

    public ShopClient(String token)
    {
        this.request = new HttpRequest(API_BASE_URL, token);
    }

    @Override
    public List<OrderResponse> getOrdersByStatus(OrderStatus status) throws IOException, ShopErrorException
    {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("status", status.name());

        return this.request.get("/order", parameters, new TypeReference<List<OrderResponse>>()
        {
        }.getType());
    }

    @Override
    public void completeAction(UUID id) throws IOException, ShopErrorException
    {
        this.request.post("/action/" + id + "/complete", null, null);
    }
}