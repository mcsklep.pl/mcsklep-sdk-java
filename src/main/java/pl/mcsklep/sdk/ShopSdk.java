package pl.mcsklep.sdk;

import pl.mcsklep.sdk.exception.ShopErrorException;
import pl.mcsklep.sdk.model.response.order.OrderResponse;
import pl.mcsklep.sdk.model.response.order.OrderStatus;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface ShopSdk
{
    String API_BASE_URL = "https://api.mcsklep.pl";

    List<OrderResponse> getOrdersByStatus(OrderStatus status) throws IOException, ShopErrorException;

    void completeAction(UUID id) throws IOException, ShopErrorException;
}
