package pl.mcsklep.sdk.exception;

public class ShopErrorException
        extends Exception
{
    private int    code;
    private String message;

    public ShopErrorException(int code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public int getCode()
    {
        return this.code;
    }

    public String getRawMessage()
    {
        return this.message;
    }

    @Override
    public String getMessage()
    {
        return String.format("%s (%s)", this.message, this.code);
    }
}
