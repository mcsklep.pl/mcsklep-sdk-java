package pl.mcsklep.sdk.model.response.action;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;

public class ActionResponse
{
    private UUID         id;
    private String       type;
    private ActionStatus status;

    private Map<String, String> parameters = new HashMap<>();

    public UUID getId()
    {
        return id;
    }

    public void setId(UUID id)
    {
        this.id = id;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public ActionStatus getStatus()
    {
        return status;
    }

    public void setStatus(ActionStatus status)
    {
        this.status = status;
    }

    @JsonAnySetter
    public void setParameter(String name, String value)
    {
        this.parameters.put(name, value);
    }

    public String getParameter(String name)
    {
        return this.parameters.get(name);
    }

    @JsonAnyGetter
    public Map<String, String> getParameters()
    {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters)
    {
        this.parameters = parameters;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", ActionResponse.class.getSimpleName() + "[", "]").add("id=" + id)
                .add("status=" + status)
                .add("parameters=" + parameters)
                .toString();
    }
}
