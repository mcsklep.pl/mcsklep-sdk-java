package pl.mcsklep.sdk.model.response.action;

public enum ActionStatus
{
    PENDING,
    COMPLETED;
}