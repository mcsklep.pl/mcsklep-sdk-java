package pl.mcsklep.sdk.model.response.order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.mcsklep.sdk.model.response.action.ActionResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderResponse
{
    private UUID id;

    private String name;

    private OrderStatus status;

    private List<ActionResponse> actions = new ArrayList<>();

    private Map<String, String> variables = new HashMap<>();

    public UUID getId()
    {
        return id;
    }

    public void setId(UUID id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public OrderStatus getStatus()
    {
        return status;
    }

    public void setStatus(OrderStatus status)
    {
        this.status = status;
    }

    public List<ActionResponse> getActions()
    {
        return actions;
    }

    public void setActions(List<ActionResponse> actions)
    {
        this.actions = actions;
    }

    public String getVariable(String name)
    {
        return this.variables.get(name);
    }

    public void setVariable(String name, String value)
    {
        this.variables.put(name, value);
    }

    public Map<String, String> getVariables()
    {
        return variables;
    }

    public void setVariables(Map<String, String> variables)
    {
        this.variables = variables;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", OrderResponse.class.getSimpleName() + "[", "]").add("id=" + id)
                .add("name='" + name + "'")
                .add("status=" + status)
                .add("actions=" + actions)
                .add("variables=" + variables)
                .toString();
    }
}
