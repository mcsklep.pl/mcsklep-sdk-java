package pl.mcsklep.sdk.model.response.order;

public enum OrderStatus
{
    PENDING,
    PAYED,
    COMPLETED;
}